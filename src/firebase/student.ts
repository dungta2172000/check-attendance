/* eslint-disable no-restricted-globals */
import { db } from './index';
import {
  collection,
  getDocs,
  addDoc,
  updateDoc,
  deleteDoc,
  doc,
  getDoc,
} from '@firebase/firestore';

const studentCollectionRef = collection(db, 'student');
const teacherCollectionRef = collection(db, 'teacher');

export type Attendance = {
  off: number;
  late: number;
  offHavePermission: number;
};
export interface IStudentBase {
  fullName: string;
  studentId: string;
  birthday: string;
  email: string;
  address: string;
}

export interface IStudentCreate extends IStudentBase {
  attendance: Attendance;
}

const defaultAttendance: Attendance = {
  off: 0,
  late: 0,
  offHavePermission: 0,
};

export const createStudent = async (studentInput: IStudentCreate) => {
  await addDoc(studentCollectionRef, { ...studentInput, attendance: { ...defaultAttendance } });
  location.reload();
};

export const updateStudent = async (id, studentInput: IStudentBase) => {
  const userDoc = doc(db, 'student', id);
  await updateDoc(userDoc, { ...studentInput });
};

export const getStudentById = async (studentid: string) => {
  const studentDoc = doc(db, 'student', studentid);
  const res = await getDoc(studentDoc);
  return { ...res.data(), id: res.id };
};

export const getListStudent = async () => {
  const data = await getDocs(studentCollectionRef);
  return data.docs.map((doc) => ({ ...doc.data(), id: doc.id }));
};

export const deleteStudent = async (id) => {
  const userDoc = doc(db, 'student', id);
  await deleteDoc(userDoc);
  location.reload();
};

export const createTeacher = async (teacherInput: any) => {
  await addDoc(teacherCollectionRef, teacherInput);
};
