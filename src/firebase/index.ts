import { initializeApp } from '@firebase/app';
import { getAuth } from '@firebase/auth';
import { getFirestore } from "@firebase/firestore";

const firebaseConfig = {
  apiKey: 'AIzaSyB6wT7rjpIptdkQY_wKEjVOYoRPMijQOxI',
  authDomain: 'check-attendance-c84f2.firebaseapp.com',
  projectId: 'check-attendance-c84f2',
  storageBucket: 'check-attendance-c84f2.appspot.com',
  messagingSenderId: '162466779159',
  appId: '1:162466779159:web:095b6838c9de131790cc52',
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getFirestore(app);

export default app;
