import { useUserAuth } from 'context/userAuthContext';
import { useContext } from 'react';
import { Navigate, Outlet, RouteProps, useLocation } from 'react-router-dom';

export const PrivateRoute = (props: RouteProps) => {
  const { user } = useUserAuth();

  let location = useLocation();
  //Check if user is logged in
  // const isLoggedIn = true;
  if (!user) return <Navigate to='/login' state={{ from: location }} />;
  //@ts-ignore
  return <Outlet {...props} />;
};
