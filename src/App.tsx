

import { NotFound, PrivateRoute } from 'components/Common';
import MenuAppBar from 'components/MenuAppBar';
import { UserAuthContextProvider } from 'context/userAuthContext';
import { CookiesProvider } from 'react-cookie';
import { Route, Routes } from 'react-router-dom';
import { AddCourse } from 'screens/AddCourse';

// import { CourseDetail } from 'screens/CourseDetail';
import { createTheme } from '@mui/material';
import { ThemeProvider } from '@mui/private-theming';
import { Register } from 'screens/Register';
import { UserManagement } from 'screens/UserManagement';
import { AttendeeSheet } from 'screens/AttendeeSheet';
import CreateInfo from 'screens/CreateInfo';
import { HomePage } from 'screens/HomePage';
import { ListInfo } from 'screens/ListInfo';
import { Login } from 'screens/Login';


const theme = createTheme({
  palette: {
    primary: {
      main: '#0B3C5D',
    },
    secondary: {
      main: '#1D2731',
    },
  },
});

function App() {
  return (
    <div className='App'>
      <ThemeProvider theme={theme}>
        <UserAuthContextProvider>
          <MenuAppBar />
          <CookiesProvider>
            <Routes>
              <Route path='/login' element={<Login />} />
              <Route path='/register' element={<Register />} />
              <Route element={<PrivateRoute />}>
                <Route path='/' element={<HomePage />} />
                <Route path='/attendeeSheet' element={<AttendeeSheet />} />
                <Route path='/create' element={<CreateInfo />} />
                <Route path='/listInfo' element={<ListInfo />} />
                <Route path='/user' element={<UserManagement />} />
                <Route path='/add' element={<AddCourse />} />
                {/* <Route path='/course/:id' element={<CourseDetail />} /> */}
              </Route>
              <Route path='*' element={<NotFound />} />
            </Routes>
          </CookiesProvider>
        </UserAuthContextProvider>
      </ThemeProvider>
    </div>
  );
}

export default App;
