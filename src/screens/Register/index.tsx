import  { useRef, useState } from 'react';
import { Box, Button, Paper, Typography } from '@mui/material/';
import * as yup from 'yup';
import { CustomTextField } from 'components/CustomTextField';
import { Formik, FormikProps } from 'formik';
// import { serialize } from 'object-to-formdata';
import { useUserAuth } from 'context/userAuthContext';
import { useStyles } from './style';
import { useNavigate } from "react-router-dom";

export interface LoginForm {
  email: string;
  password: string;
}

const schema = yup.object().shape({
  email: yup.string().required('Vui lòng nhập email đăng kí'),
  password: yup.string().required('Vui lòng nhập mật khẩu'),
});

export const Register = () => {
  const classes = useStyles();
  const form = useRef<FormikProps<LoginForm> | null>(null);
  const formData = useRef<LoginForm>({
    email: '',
    password: '',
  });
  const [errorSignUp, setErrorSignUp] = useState<string>('');
  const { signUp } = useUserAuth();
  let navigate = useNavigate();

  const onSubmit = async (form: LoginForm) => {
    console.log('click');

    const { email, password } = form;
    try {
      await signUp(email, password);
      navigate('/');
    } catch (err) {
      setErrorSignUp(err.message);
    }
    console.log(form);
  };

  return (
    <div className={classes.root}>
      <Paper elevation={1} className={classes.box}>
        <Typography variant='h5' component='h1' align='center' mb={2}>
          Đăng ký
        </Typography>
        <Formik
          validationSchema={schema}
          initialValues={formData.current}
          onSubmit={onSubmit}
          innerRef={(instance) => (form.current = instance)}
          validateOnMount
        >
          {({ handleChange, handleBlur, handleSubmit, values, errors, isValid, touched }) => (
            <Box component='form' autoComplete='off'>
              <CustomTextField
                title='Email'
                hint='Nhập email'
                onBlur={handleBlur('email')}
                onTextChange={handleChange('email')}
                error={touched.email && errors.email !== undefined}
                value={values.email}
                errorText={errors.email as string}
              />
              {/* <CustomTextField
                title='Tên đăng nhập'
                hint='Nhập tên đăng nhập'
                onBlur={handleBlur('email')}
                onTextChange={handleChange('email')}
                error={touched.email && errors.email !== undefined}
                value={values.email}
                errorText={errors.email}
              /> */}

              <CustomTextField
                title='Password'
                hint='Nhập mật khẩu'
                onBlur={handleBlur('password')}
                onTextChange={handleChange('password')}
                error={touched.password && errors.password !== undefined}
                value={values.password}
                errorText={errors.password as string}
              />
              {/* <CustomTextField
                title='Họ và tên'
                hint='Nhập họ và tên'
                onBlur={handleBlur('name')}
                onTextChange={handleChange('name')}
                error={touched.password && errors.password !== undefined}
                value={values.password}
                errorText={errors.password}
              /> */}
              {/* <CustomTextField
                title='Checkbox'
                hint='Nhập email'
                onBlur={handleBlur('email')}
                onTextChange={handleChange('email')}
                error={touched.password && errors.password !== undefined}
                value={values.password}
                errorText={errors.password}
              /> */}

              <Box mt={4}>
                <Button
                  disabled={!isValid}
                  fullWidth
                  variant='contained'
                  color={isValid ? 'primary' : 'secondary'}
                  onClick={() => handleSubmit()}
                >
                  Login
                </Button>
              </Box>
            </Box>
          )}
        </Formik>
        <div style={{color : 'red'}}>{errorSignUp.length ? <div>{errorSignUp}</div> : null }</div>
      </Paper>
    </div>
  );
};
