/* eslint-disable no-restricted-globals */
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import {
  Box,
  Dialog,
  DialogContent,
  DialogTitle,
  Divider,
  Fab,
  Paper,
  Stack,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tabs,
  Toolbar,
  Typography,
  Checkbox,
  Button,
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import { CustomTextField } from 'components/CustomTextField';
import ScrollTopButton from 'components/ScrollTopButton';
import { getListStudent, IStudentCreate, updateStudent } from 'firebase/student';
import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { useAppSelector } from 'redux/hooks';
import { RootState } from 'redux/stores';
import { UserDetail } from 'screens/UserDetail';
import produce from 'immer';
interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

const useStyles = makeStyles({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    width: '100%',
    padding: 20,
    flexGrow: 1,
    alignItems: 'center'
  },
  tableContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
  },
  status: {
    padding: 7,
    backgroundColor: '#D2EBD3',
    display: 'inline-block',
    borderRadius: 50,
    color: '#9BD39E',
    fontWeight: 'bold',
  },
  active: {
    padding: 7,
    backgroundColor: '#D2EBD3',
    display: 'inline-block',
    borderRadius: 50,
    color: '#9BD39E',
    fontWeight: 'bold',
  },
  inActive: {
    padding: 7,
    backgroundColor: '#DC0005',
    display: 'inline-block',
    borderRadius: 50,
    color: '#FF8B8E',
    fontWeight: 'bold',
  },
  toolbar: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  fab: {
    position: 'absolute',
    bottom: 16,
    right: 16,
    zIndex: 100,
  },
});

const TabPanel = (props: TabPanelProps) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
};

interface CourseDetailProps { }

export const AttendeeSheet = (props: CourseDetailProps) => {
  const classes = useStyles();
  const [userDialog, setUserDialog] = useState(false);
  const data = useAppSelector((state: RootState) => state.course.courseDetail);

  const [tab, setTab] = useState(0);
  const handleChangeTab = (event: React.SyntheticEvent, value: number) => {
    setTab(value);
  };

  const [loading, setLoading] = useState(true);
  // const [userData, setUserData] = useState([]);
  const [currentUser, setCurrentUser] = useState();
  const location1 = useLocation();

  const [userData, setUserData] = useState<IStudentCreate[]>([]);

  useEffect(() => {
    setLoading(false);
    try {
      const getStudent = async () => {
        const res = await getListStudent();

        if (res.length) {
          setUserData(res as any);
        }
      };
      getStudent();
    } catch (e) {
      console.log(e);
    }
  }, []);

  const closeDialog = () => {
    setUserDialog(false);
  };

  const UserDetailDialog = () => (
    <Dialog open={userDialog} onClose={closeDialog}>
      <DialogTitle sx={{ textAlign: 'center' }}>Thông tin người dùng</DialogTitle>
      <DialogContent>
        <UserDetail onClose={closeDialog} data={currentUser} />
      </DialogContent>
    </Dialog>
  );

  if (loading) {
    return <div>Loading</div>;
  }

  const saveCheck = async () => {
    const data = JSON.parse(JSON.stringify(userData));

    for (let user of data) {
      const idStudent = (user as any).id;
      delete (user as any).id;
      await updateStudent(idStudent, user);
    }
    location.reload();
  };

  return (
    <React.Fragment>
      <Paper sx={{ display: 'flex', position: 'relative', px: 7, mt: 3 }}>
        <UserDetailDialog />
        <Paper className={classes.container} component='main'>
          <Typography variant='h3' gutterBottom component='div'>
            Bảng điểm danh
          </Typography>
          <Paper sx={{ width: '100%' }}>
            <Toolbar
              sx={{
                pl: { sm: 2 },
                pr: { xs: 1, sm: 1 },
              }}
            >
              <Stack
                direction={{ xs: 'column', sm: 'row' }}
                spacing={{ xs: 1, sm: 2, md: 4 }}
                className={classes.toolbar}
              >
                <Typography variant='h6' id='tableTitle' component='div'>
                  Tổng số học viên: {userData.length}
                </Typography>
              </Stack>
            </Toolbar>
            <TableContainer component={Paper} sx={{ mt: 2, maxHeight: 500 }}>
              <Divider />
              <Table stickyHeader aria-label='sticky table'>
                <TableHead>
                  <TableRow>
                    <TableCell align='center'>STT</TableCell>
                    <TableCell align='center'>Họ và tên</TableCell>
                    <TableCell align='center'>Nghỉ không phép</TableCell>
                    <TableCell align='center'>Vào muộn</TableCell>
                    <TableCell align='center'>Nghỉ có phép</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {userData.map((user, index) => (
                    <TableRow hover key={index}>
                      <TableCell align='center'>
                        <Typography>{user.studentId}</Typography>
                      </TableCell>
                      <TableCell align='center'>
                        <Typography>{user.fullName}</Typography>
                      </TableCell>
                      <TableCell align='center'>
                        <CustomTextField
                          title=''
                          styles={{ maxWidth: 50 }}
                          onTextChange={(e) => {
                            setUserData(
                              produce((draft) => {
                                draft[index].attendance.off = Number(e.target.value);
                              })
                            );
                          }}
                          value={user.attendance.off.toString()}
                        />
                      </TableCell>
                      <TableCell align='center'>
                        <CustomTextField
                          title=''
                          styles={{ maxWidth: 50 }}
                          onTextChange={(e) => {
                            setUserData(
                              produce((draft) => {
                                draft[index].attendance.late = Number(e.target.value);
                              })
                            );
                          }}
                          value={user.attendance.late.toString()}
                        />
                      </TableCell>
                      <TableCell align='center'>
                        <CustomTextField
                          title=''
                          styles={{ maxWidth: 50 }}
                          onTextChange={(e) => {
                            setUserData(
                              produce((draft) => {
                                draft[index].attendance.offHavePermission = Number(
                                  e.target.value
                                );
                              })
                            );
                          }}
                          value={user.attendance.offHavePermission.toString()}
                        />
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
          <br />
          <div style={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
            <Button onClick={saveCheck} variant='contained'>
              Xác nhận
            </Button>
          </div>
        </Paper>
      </Paper>
      <ScrollTopButton {...props}>
        <Fab color='primary' size='medium' aria-label='scroll back to top'>
          <KeyboardArrowUpIcon />
        </Fab>
      </ScrollTopButton>
    </React.Fragment >
  );
};
