import { makeStyles } from '@mui/styles';
import { Button, Box, styled, Typography, Stack } from '@mui/material';
import { Formik, FormikProps } from 'formik';
import * as yup from 'yup';
import { MouseEventHandler, useEffect, useRef } from 'react';
import { CustomTextField } from 'components/CustomTextField';
import PhotoCameraIcon from '@mui/icons-material/PhotoCamera';
import MovieCreationIcon from '@mui/icons-material/MovieCreation';
import { createStudent, updateStudent } from 'firebase/student';

export interface CourseForm {
  studentId?: string;
  fullName?: string;
  birthday?: string;
  email?: string;
  address?: string;
}

export interface AddCourseProps {
  onClose?: () => void;
  onSubmit?: (value: CourseForm) => void;
  student?: any;
}

const schema = yup.object().shape({
  studentId: yup.string().required('Vui lòng nhập mã sinh viên'),
  fullName: yup.string().required('Vui lòng nhập họ tên'),
  birthday: yup.string().required('Vui lòng nhập ngày sinh'),
  email: yup.string().email().required('Vui lòng nhập email'),
});

export const AddInfo = (props: AddCourseProps) => {
  const form = useRef<FormikProps<CourseForm> | null>(null);
  const formData = useRef<CourseForm>(props.student ? props.student : {});

  const handleSubmit = async (value) => {
    const idStudent = value.id;
    delete value.id;

    try {
      if (props.student) {
        await updateStudent(idStudent, value);
        // eslint-disable-next-line no-restricted-globals
        location.reload();
      } else {
        await createStudent(value);
      }
      props.onClose();
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Formik
      validationSchema={schema}
      initialValues={formData.current}
      onSubmit={handleSubmit}
      innerRef={(instance) => (form.current = instance)}
    >
      {({ handleChange, handleBlur, handleSubmit, values, errors, isValid, touched }) => (
        <Box component='form' autoComplete='off'>
          <CustomTextField
            title='Họ và tên*'
            hint='Nhập họ và tên'
            onBlur={handleBlur('fullName')}
            onTextChange={handleChange('fullName')}
            error={touched.fullName && errors.fullName !== undefined}
            value={values.fullName}
            errorText={errors.fullName as string}
          />
          <CustomTextField
            title='Mã sinh viên*'
            hint='Nhập mã sinh viên'
            onBlur={handleBlur('studentId')}
            onTextChange={handleChange('studentId')}
            error={touched.studentId && errors.studentId !== undefined}
            value={values.studentId}
            errorText={errors.studentId as string}
          />
          <CustomTextField
            title='Ngày sinh*'
            hint='Nhập ngày sinh'
            onBlur={handleBlur('birthday')}
            onTextChange={handleChange('birthday')}
            value={values.birthday}
            error={touched.birthday && errors.birthday !== undefined}
            errorText={errors.birthday as string}
          />
          <CustomTextField
            title='Email*'
            hint='Enter email'
            onBlur={handleBlur('email')}
            onTextChange={handleChange('email')}
            value={values.email}
            error={touched.email && errors.email !== undefined}
            errorText={errors.email as string}
          />
          <CustomTextField
            title='Address'
            hint='Enter address'
            onBlur={handleBlur('address')}
            onTextChange={handleChange('address')}
            value={values.address}
            error={touched.address && errors.address !== undefined}
            errorText={errors.address as string}
          />
          {/* <div className={classes.row}>
            <Box>
              <Typography variant='subtitle2' color='text.secondary' textAlign='center'>
                Ảnh bìa
              </Typography>
              <label htmlFor='thumbnail'>
                <Input accept='image/*' id='thumbnail' type='file' />
                <Button variant='contained' component='span' startIcon={<PhotoCameraIcon />}>
                  Thêm ảnh
                </Button>
              </label>
            </Box>
            <Box>
              <Typography variant='subtitle2' color='text.secondary' textAlign='center'>
                Video giới thiệu
              </Typography>
              <label htmlFor='video'>
                <Input accept='video/*' id='video' type='file' />
                <Button variant='contained' component='span' startIcon={<MovieCreationIcon />}>
                  Thêm video
                </Button>
              </label>
            </Box>
          </div> */}
          <Stack spacing={10} direction='row' sx={{ justifyContent: 'center', mt: 5 }}>
            <Button onClick={props.onClose}>Hủy bỏ</Button>
            <Button
              onClick={
                handleSubmit as unknown as MouseEventHandler<HTMLButtonElement>
                // () => console.log('123')
              }
              variant='contained'
            >
              Xác nhận
            </Button>
          </Stack>
        </Box>
      )}
    </Formik>
  );
};
