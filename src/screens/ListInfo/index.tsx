import { useState, useEffect } from 'react';
import {
  Button,
  Stack,
  Typography,
  Grid,
  Box,
  Dialog,
  DialogTitle,
  DialogContent,
  Backdrop,
  CircularProgress,
  TableRow,
  TableCell,
  TableContainer,
  Divider,
  Toolbar,
  Paper,
  Table,
  TableHead,
  TableBody,
  Checkbox,
  IconButton,
} from '@mui/material';
import AddRoundedIcon from '@mui/icons-material/AddRounded';
import SaveAltRoundedIcon from '@mui/icons-material/SaveAltRounded';
import { AddCourse } from 'screens/AddCourse';
import FileUploadOutlinedIcon from '@mui/icons-material/FileUploadRounded';
import FilterListRoundedIcon from '@mui/icons-material/FilterListRounded';
import { useAppSelector } from 'redux/hooks';
import { RootState } from '../../redux/stores';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
// import { getListCourse } from './actions';
import { useDispatch } from 'react-redux';
import CourseContainer from '../../components/CourseContainer';
import { AddInfo } from 'screens/AddInfo';
import { makeStyles } from '@mui/styles';
import { deleteStudent, getListStudent, IStudentCreate } from 'firebase/student';

const useStyles = makeStyles({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignitems: 'center',
    width: '100%',
  },
  courses: {
    display: 'inline-flex',
    flexWrap: 'wrap',
    width: '100%',
    margin: 'auto',
  },
  function: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 20,
    paddingLeft: 15,
    paddingRight: 15,
  },
  tableContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
  },
  status: {
    padding: 7,
    backgroundColor: '#D2EBD3',
    display: 'inline-block',
    borderRadius: 50,
    color: '#9BD39E',
    fontWeight: 'bold',
  },
  active: {
    padding: 7,
    backgroundColor: '#D2EBD3',
    display: 'inline-block',
    borderRadius: 50,
    color: '#9BD39E',
    fontWeight: 'bold',
  },
  inActive: {
    padding: 7,
    backgroundColor: '#DC0005',
    display: 'inline-block',
    borderRadius: 50,
    color: '#FF8B8E',
    fontWeight: 'bold',
  },
  toolbar: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  fab: {
    position: 'absolute',
    bottom: 16,
    right: 16,
    zIndex: 100,
  },
});

export const ListInfo = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const data = useAppSelector((state: RootState) => state.course.listCourse);
  const [loading, setLoading] = useState(true);
  const [dialog, setDialog] = useState(false);
  const [editDialog, setEditDialog] = useState(false);
  const [deleteDialog, setDeleteDialog] = useState(false);
  const [userData, setUserData] = useState<IStudentCreate[]>([]);
  const [currentStudent, setCurrentStudent] = useState(-1);

  useEffect(() => {
    // if (data == null) {
    //   dispatch(getListCourse());
    // }
    setLoading(false);
  }, []);

  useEffect(() => {
    setLoading(false);

    try {
      const getStudent = async () => {


        const res = await getListStudent();
        if (res.length) {
          setUserData(res as any);
        }
      };
      getStudent();
    } catch (e) {
      console.log(e);
    }
  }, []);

  const closeDialog = () => {
    setDialog(false);
  };

  const openDialog = () => {
    setDialog(true);
  };

  const handleOpenEditDialog = () => {
    setEditDialog(true);
  };

  const handleCloseEditDialog = () => {
    setEditDialog(false);
    setCurrentStudent(-1);
  };

  const closeDeleteDialog = () => {
    setDeleteDialog(false);
    setCurrentStudent(-1);
  };

  const openDeleteDialog = () => {
    setDeleteDialog(true);
  };

  const AddCourseDialog = () => (
    <Dialog open={dialog} onClose={closeDialog}>
      <DialogTitle sx={{ textAlign: 'center' }}>Thêm học viên</DialogTitle>
      <DialogContent>
        <AddInfo onClose={closeDialog} onSubmit={(value: any) => console.log(value)} />
      </DialogContent>
    </Dialog>
  );

  const EditCourseDialog = () => (
    <Dialog open={editDialog} onClose={handleCloseEditDialog}>
      <DialogTitle sx={{ textAlign: 'center' }}>Sửa thông tin học viên</DialogTitle>
      <DialogContent>
        <AddInfo
          onClose={handleCloseEditDialog}
          onSubmit={(value: any) => console.log(value)}
          student={userData[currentStudent]}
        />
      </DialogContent>
    </Dialog>
  );

  const DeleteDialog = () => (
    <Dialog open={deleteDialog} onClose={closeDeleteDialog}>
      <DialogTitle sx={{ textAlign: 'center' }}>Xóa học viên</DialogTitle>
      <DialogContent>
        <Typography>Bạn có chắc chắn muốn xóa học viên này?</Typography>
        <Stack spacing={10} direction='row' sx={{ justifyContent: 'center', mt: 5 }}>
          <Button onClick={closeDeleteDialog}>Hủy bỏ</Button>
          <Button
            variant='contained'
            onClick={async () => {
              await deleteStudent((userData[currentStudent] as any).id);
              // console.log(userData[currentStudent]);

              closeDeleteDialog();
            }}
          >
            Xác nhận
          </Button>
        </Stack>
      </DialogContent>
    </Dialog>
  );

  if (loading) {
    return (
      <Box
        sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh' }}
      >
        <CircularProgress />
      </Box>
    );
  }

  return (
    <div className={classes.container}>
      <Stack direction='row' flexWrap='wrap' className={classes.function}>
        <Stack direction='row' spacing={5} flexWrap='wrap'>
          <Button variant='contained' endIcon={<AddRoundedIcon />} onClick={openDialog}>
            Thêm học viên
          </Button>
          <AddCourseDialog />
          <EditCourseDialog />
          <DeleteDialog />
          <Button
            variant='outlined'
            endIcon={<FileUploadOutlinedIcon />}
            onClick={() => console.log('123')}
          >
            Nhập file CSV
          </Button>
          <Button variant='outlined' endIcon={<SaveAltRoundedIcon />}>
            Xuất file CSV
          </Button>
        </Stack>
        <Stack>
          <Button variant='contained' endIcon={<FilterListRoundedIcon />}>
            Lọc
          </Button>
        </Stack>
      </Stack>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
          {loading ? (
            <Backdrop
              sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
              open={loading}
            >
              <CircularProgress color='inherit' />
            </Backdrop>
          ) : (
            <Paper sx={{ width: '100%', px: 10, height: '100vh' }}>
              <Typography variant='h3' gutterBottom component='div' style={{ marginTop: 100, textAlign: 'center' }}>Danh sách sinh viên</Typography>
              <Toolbar
                sx={{
                  pl: { sm: 2 },
                  pr: { xs: 1, sm: 1 },
                }}
              >
                <Typography mt={5} variant='h6' id='tableTitle' component='div'>
                  Tổng số học viên: {userData.length}
                </Typography>
              </Toolbar>
              <TableContainer component={Paper} sx={{ mt: 2, maxHeight: 500 }}>
                <Divider />
                <Table stickyHeader aria-label='sticky table'>
                  <TableHead>
                    <TableRow>
                      <TableCell align='center'>MSSV</TableCell>
                      <TableCell align='center'>Họ và tên</TableCell>
                      <TableCell align='center'>Ngày sinh</TableCell>
                      <TableCell align='center'>Địa chỉ</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {userData.map((user, index) => (
                      <TableRow hover key={index}>
                        <TableCell align='center'>
                          <Typography>{user.studentId}</Typography>
                        </TableCell>
                        <TableCell align='center'>
                          <Typography>{user.fullName}</Typography>
                        </TableCell>
                        <TableCell align='center'>
                          <Typography>{user.birthday}</Typography>
                        </TableCell>
                        <TableCell align='center'>
                          <Typography>{user.address}</Typography>
                        </TableCell>
                        <TableCell align='center'>
                          <IconButton color='primary' aria-label='center' component='span'>
                            <EditOutlinedIcon
                              onClick={() => {
                                handleOpenEditDialog();
                                setCurrentStudent(index);
                              }}
                            />
                          </IconButton>
                          <IconButton color='primary' aria-label='center' component='span'>
                            <DeleteOutlineOutlinedIcon
                              onClick={() => {
                                openDeleteDialog();
                                setCurrentStudent(index);
                              }}
                            />
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Paper>
          )}
        </Grid>
      </Box>
    </div>
  );
};
