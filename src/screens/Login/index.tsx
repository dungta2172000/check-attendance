import { useRef, useState } from 'react';
import { Box, Button, Paper, Typography } from '@mui/material/';
// import { makeStyles } from '@mui/styles';
import * as yup from 'yup';
import { CustomTextField } from 'components/CustomTextField';
import { Formik, FormikProps } from 'formik';
import { useNavigate } from 'react-router-dom';
// import { login } from './actions';
// import { useDispatch, useSelector } from 'react-redux';
// import { useAppSelector } from 'redux/hooks';
// import { RootState } from 'redux/stores';
import { useStyles } from './style';
import { useUserAuth } from 'context/userAuthContext';

export interface LoginForm {
  email: string;
  password: string;
}

const schema = yup.object().shape({
  email: yup.string().required('Vui lòng nhập email'),
  password: yup.string().required('Vui lòng nhập mật khẩu'),
});

export const Login = () => {
  const classes = useStyles();
  // const loginData = useAppSelector((state: RootState) => state.app.loggedIn);
  const navigate = useNavigate();
  const form = useRef<FormikProps<LoginForm> | null>(null);
  const formData = useRef<LoginForm>({
    email: '',
    password: '',
  });
  const [errorLogIn, setErrorSignUp] = useState<string>('');
  const { logIn } = useUserAuth();

  const goToRegister = () => {
    navigate(`/register`);
  };

  const onSubmit = async (form: LoginForm) => {
    setErrorSignUp('');
    const { email, password } = form;
    try {
      await logIn(email, password);
      navigate('/');
    } catch (err) {
      setErrorSignUp(err.message);
    }
  };

  // useEffect(() => {
  //   if (loginData) {
  //     console.log('logged in');

  //     navigate('/');
  //   }
  // }, [loginData])

  return (
    <div className={classes.root}>
      <Paper elevation={1} className={classes.box}>
        <Typography variant='h5' component='h1' align='center' mb={2}>
          Đăng nhập
        </Typography>
        <Formik
          validationSchema={schema}
          initialValues={formData.current}
          onSubmit={onSubmit}
          innerRef={(instance) => (form.current = instance)}
          validateOnMount
        >
          {({ handleChange, handleBlur, handleSubmit, values, errors, isValid, touched }) => (
            <Box component='form' autoComplete='off'>
              <CustomTextField
                title='Tên đăng nhập'
                hint='Nhập tên đăng nhập'
                onBlur={handleBlur('email')}
                onTextChange={handleChange('email')}
                error={touched.email && errors.email !== undefined}
                value={values.email}
                errorText={errors.email}
              />
              <CustomTextField
                title='Mật khẩu'
                hint='Nhập mật khẩu'
                onBlur={handleBlur('password')}
                onTextChange={handleChange('password')}
                error={touched.password && errors.password !== undefined}
                value={values.password}
                errorText={errors.password}
                isPassword={true}
              />

              <Box mt={4}>
                <Button
                  disabled={!isValid}
                  fullWidth
                  variant='contained'
                  color={isValid ? 'primary' : 'secondary'}
                  onClick={() => handleSubmit()}
                >
                  Login
                </Button>
              </Box>
            </Box>
          )}
        </Formik>
        <Button onClick={goToRegister}>
          <Typography variant='body2' align='center' mt={5}>
            Chưa có tài khoản? Đăng ký ngay
          </Typography>
        </Button>
        <div style={{ color: 'red' }}>{errorLogIn.length ? <div>{errorLogIn}</div> : null}</div>
      </Paper>
    </div>
  );
};
