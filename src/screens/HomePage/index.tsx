import React from 'react';
import { DrawerMenu } from 'components/DrawerMenu';
import { Box, Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { CustomCard } from 'components/CustomCard';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexFlow: 'row',
  },

  detailPage: {
    display: 'flex',
    flexFlow: 'row wrap',
    paddingLeft: 20,
    paddingTop: 30,
  },
  tableContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
  },
  table: {
    flex: 1,
    paddingLeft: 20,
  },
});

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

const TabPanel = (props: TabPanelProps) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
};

export const HomePage = () => {
  const [tab, setTab] = React.useState(0);
  const handleChangeTab = (event: React.SyntheticEvent, value: number) => {
    setTab(value);
  };

  const classes = useStyles();

  return (
    <div>
      <div className={classes.root}>
        <DrawerMenu type={2} />
        <div className={classes.detailPage}>
          <CustomCard
            heading='Điểm danh'
            // title='20'
            // subtitle='khóa'
            buttonText='Xem chi tiết'
            page='attendeeSheet'
          />
          <CustomCard
            heading='Danh sách sinh viên'
            // title='20'
            // subtitle='người'
            buttonText='Xem chi tiết'
            page='listInfo'
          />
        </div>
      </div>
    </div>
  );
};
